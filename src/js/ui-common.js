var CmpayUI = window.CmpayUI || {};

/*
 * 参数为jquery 对象
 */
CmpayUI.disable = function (ui)
{
	var className = $(ui).attr("class");
	
	// 按钮
	if (className.indexOf('u-btn') >= 0)
	{
		var classFlag = className.indexOf("u-btn-wh") >= 0 ? "u-btn-wh"  : className.indexOf("u-btn-b") >= 0 ? "u-btn-b" : "u-btn";
		$(ui).addClass(classFlag + "-disabled");
	}
};

/*
 * 参数为jquery 对象
 */
CmpayUI.enable = function (ui)
{
	var className = $(ui).attr("class");
	
	// 按钮
	if (className.indexOf('u-btn') >= 0)
	{
		var classFlag = className.indexOf("u-btn-wh") >= 0 ? "u-btn-wh"  : className.indexOf("u-btn-b") >= 0 ? "u-btn-b" : "u-btn";
		$(ui).removeClass(classFlag + "-disabled");
	}
};

/**
 * 按钮组
 */
CmpayUI.ButtonGroup =
{
	init: function (groupId, values, defaultNum)
	{
		defaultNum >= 1 ? defaultNum : 1;
		
		var objs = $("a[group=" + groupId + "]");
		
		for (var j = 0; j < objs.length; j++)
		{
			// 设置默认选中按钮
			var e = objs[j];
			if ($(e).attr("class").indexOf("u-link-db-hover") >= 0 && j +1 != defaultNum)
			{
				$(e).removeClass("u-link-db-hover");
			};
			
			if (j + 1 == defaultNum)
			{
				$(e).addClass("u-link-db-hover");
				$("input[group=" + groupId + "]").val(values[j]);
			}
			
			$(e).attr("val", values[j]).click(function ()
			{
				var objsTemp = $("a[group=" + groupId + "]");
				for (var k = 0; k < objsTemp.length; k++)
				{
					$(objsTemp[k]).removeClass("u-link-db-hover");
				}
				
				$(this).addClass("u-link-db-hover");
				$("input[group=" + groupId + "]").val($(this).attr("val"));
			});
		}
	},
	
	// 获取UI组选择值
	val: function (groupId)
	{
		return $("input[group=" + groupId + "]").val();
	}
};

/**
 * 步骤条
 * @type 
 */
CmpayUI.StepBar =
{
    init: function (groupId, values, defaultNum)
	{
		defaultNum >= 1 ? defaultNum : 1;
		
		var objs = $("div[group=" + groupId + "]");
		
		for (var j = 0; j < objs.length; j++)
		{
			// 设置已完成步骤
			var e = objs[j];
			$(e).html(j + 1).next().html(values[j]);
			
			// 第一步始终为完成状态
			if (j == 0)
			{
				continue;
			}
			
			if (j + 1 <= defaultNum)
			{
				if (j + 1 == objs.length) // 最后步骤
				{
					$(e).removeClass("u-step-bar-l-disable").addClass("u-step-bar-l-enable");
				}
				else  // 中间步骤
				{
					$(e).removeClass("u-step-bar-m-disable").addClass("u-step-bar-m-enable");
				}
				
				// 设置字体颜色
				$(e).next().removeClass("u-step-bar-text-g").addClass("u-step-bar-text-y");
			}
		}
	},
	val: function (groupId, index)
	{
		index >= 1 ? index : 1;
		
		var objs = $("div[group=" + groupId + "]");
		
		for (var j = 0; j < objs.length; j++)
		{
			// 设置已完成步骤
			var e = objs[j];
			
			// 第一步始终为完成状态
			if (j == 0)
			{
				continue;
			}
			
			if (j + 1 <= index)
			{
				if (j + 1 == objs.length) // 最后步骤
				{
					$(e).removeClass("u-step-bar-l-disable").addClass("u-step-bar-l-enable");
				}
				else  // 中间步骤
				{
					$(e).removeClass("u-step-bar-m-disable").addClass("u-step-bar-m-enable");
				}
				
				// 设置字体颜色
				$(e).next().removeClass("u-step-bar-text-g").addClass("u-step-bar-text-y");
			}
		    else
		    {
		        if (j + 1 == objs.length) // 最后步骤
				{
					$(e).removeClass("u-step-bar-l-enable").addClass("u-step-bar-l-disable");
				}
				else  // 中间步骤
				{
					$(e).removeClass("u-step-bar-m-enable").addClass("u-step-bar-m-disable");
				}
				
				$(e).next().removeClass("u-step-bar-text-y").addClass("u-step-bar-text-g");
		}
		}
	}
};

/**
 * 无状态步骤条
 * @type 
 */
CmpayUI.StepBarNoStatus =
{
    init: function (groupId, values)
	{
		var objs = $("div[group=" + groupId + "]");
		
		for (var j = 0; j < objs.length; j++)
		{
			// 设置文字内容
			var e = objs[j];
			$(e).html(j + 1).next().html(values[j]);
		}
	}
};

/**
 * 设置单选框
 */
jQuery.fn.setRadio = function ()
{
    $(":input[type=radio] + label").each(function ()
    {
        if ($(this).prev()[0].checked)
        {
        	$(this).removeClass("over");
            $(this).addClass("checked");
        }
    })
        .hover(
            function ()
            {
            	if ($(this).prev()[0].checked)
            	{
            		$(this).removeClass("over");
            	}
            	else
            	{
                    $(this).addClass("over");
            	}
            },
            function ()
            {
                $(this).removeClass("over");
            }
    )
        .click(function ()
        {
//            var contents = $(this).parent().parent(); /*多组控制 关键*/
            $(":input[type=radio] + label").each(function ()
            {
                $(this).prev()[0].checked = false;
                $(this).removeClass("checked");
            });
            $(this).prev()[0].checked = true;
            $(this).removeClass("over");
            $(this).addClass("checked");
        })
        .prev().hide();
}; 

/**
 * 设置复选框
 */
jQuery.fn.setCheckBox = function ()
{
    $(":input[type=checkbox] + label").each(function ()
    {
        if ($(this).prev()[0].checked)
        {
        	$(this).removeClass("over");
            $(this).addClass("checked");
        }
    })
        .hover(
            function ()
            {
            	if ($(this).prev()[0].checked)
            	{
            		$(this).removeClass("over");
            	}
            	else
            	{
                    $(this).addClass("over");
            	}
            },
            function ()
            {
                $(this).removeClass("over");
            }
    )
        .click(function ()
            {
            	if ($(this).prev()[0].checked == true)
            	{
            		$(this).removeClass("checked").addClass("over");
            		$(this).prev()[0].checked = false;
            	}
            	else
            	{
                    $(this).removeClass("over").addClass("checked");
                    $(this).prev()[0].checked = true;
            	}
            })
            .prev().hide();
};

/**
 * 浮动框
 * @type 
 */
CmpayUI.FloatLayer =
{
	init: function (options)
	{
		if (!options.id || !options.target)
		{
			alert("参数配置错误");
		}
		
		$("#"+options.id).remove();
		
		var width = options.width || 220;
		var height = options.height || 40;
		var top = options.top || 0;
		var left = options.left || 0;
		var content = options.content || "";
		
	    var html = '<div id="' + options.id + '" class="u-flayer-box" style="width: ' + width + 'px; height:' + height + 'px;display:none;"><div><div class="u-flayer-top1" style="width:'
	        + (width - 10) / 2 + 'px;">&nbsp;</div><div class="u-flayer-top2" style="width:'
	        + (width -7) / 2 + 'px">&nbsp;</div></div><div class="u-flayer-cont">'
	        +  content + '</div>';
        var objElement = $("<div>").html(html);
        
		objElement.css(
		{
		    'background': 'url(../images/dialog_arrow.png) no-repeat scroll ' + (width / 2 - 9) + 'px 0 transparent',
		    'overflow': 'hidden',
		    'position': 'absolute',
		    'z-index': '9999',
		    top: $("#" + options.target).offset().top + top,
		    left: $("#" + options.target).offset().left + left
		}).appendTo("body").fadeIn(200)
		.hover(
		function ()
		{
		    $("#" + options.id).show();
		},
		function ()
		{
		    $("#" + options.id).hide();
		});
		
		$("#" + options.target).hover(
		function ()
		{
		    $("#" + options.id).show();
		},
		function ()
		{
		    $("#" + options.id).hide();
		});
	}
};

/**
 * 设置日期控件
 * @type 
 */
CmpayUI.SelectDate =
{
    init: function (containerId)
	{
		var html = '<div class="remind_choise input_txt_info">'
		    + '<span class="remind_choise_txt" id="remindChoiseInit" style="display: none;">不提醒</span>'
            + '<div style="" class="remindDate" id="remindDateId"><a hidefocus="true" href="javascript:void(0);" class="date">每月<span id="rmd_date" class="c_orange">1</span><span id="rmd_date_hid" style="display:none"></span>日</a></div>'
            + '<span id="panel-arrow1" class="panel-arrow-down"></span>'
            + '</div>'
            + '<div style="" id="setRmindDateDiv" class="setRmindDateDiv">'
			+ '<div style="display: none;" id="dateContentId" class="date_content">'
			+ '<table id="m-generate"><tbody><tr><th colspan="7">请选择每月的提醒日</th></tr><tr><td name="1">1</td><td name="2">2</td><td name="3" class="">3</td><td name="4" class="">4</td><td name="5" class="">5</td><td name="6">6</td><td name="7">7</td></tr><tr><td name="8" class="">8</td><td name="9" class="">9</td><td name="10" class="">10</td><td name="11" class="">11</td><td name="12">12</td><td name="13">13</td><td name="14">14</td></tr><tr><td name="15">15</td><td name="16">16</td><td name="17" class="">17</td><td name="18">18</td><td name="19">19</td><td name="20">20</td><td name="21">21</td></tr><tr><td name="22">22</td><td name="23" class="">23</td><td name="24" class="">24</td><td name="25">25</td><td name="26">26</td><td name="27">27</td><td name="28">28</td></tr><tr class="no_remind"><td id="remindCancel" name="0" colspan="7" class="">不提醒</td></tr></tbody></table>'
			+ '</div>'
			+ '</div>';
		$("#" + containerId).html(html);
		$("#" + containerId).find(".remind_choise").click(setRemindDate);
		
		
		function setRemindDate ()
		{
		     $('#myCreditBox').hide();
		     $("#remindChoiseInit").hide();
		     $("#remindDateId").show();
		     $("#setRmindDateDiv").show();
		     $("#fakabank").hide();
		     $("#panel-arrow").removeClass();
		     $("#panel-arrow").addClass("panel-arrow-down");
		     popDateList();
		 };
		 
		function popDateList ()
		{
		     $("#remindDateId").show();
		     var dateContent = $("#dateContentId");
		     $("#rmd_date").text("1");
		     if (dateContent.is(":hidden"))
		     {
		         dateContent.show();
		         $("#panel-arrow1").removeClass();
		         $("#panel-arrow1").addClass("panel-arrow-up");
		         minit();
		         $("td[name]", dateContent).click(function ()
		         {
		             var text_date = $(this).text();
		             if (text_date == "不提醒")
		             {
		                 $("#remindChoiseInit").show();
		                 $("#remindDateId").hide();
		                 $("#rmd_date").text("");
		                 $("#rem_dt").val("");
		                 $("#dateContentId").hide();
		             }
		             else
		             {
		                 $("#rmd_date").text(text_date);
		                 //被选择了的日期
		             }
		             dateContent.hide();
		             $("#panel-arrow1").removeClass();
		             $("#panel-arrow1").addClass("panel-arrow-down");
		         });
		         $("#setRmindDateDiv").mouseleave(function ()
		         {
		             dateContent.hide();
		             $("#panel-arrow1").removeClass();
		             $("#panel-arrow1").addClass("panel-arrow-down");
		         });
		     }
		     else
		     {
		         dateContent.hide();
		         $("#panel-arrow1").removeClass();
		         $("#panel-arrow1").addClass("panel-arrow-down");
		     }
		 };
		 
		 //对td添加样式
		 function minit ()
		 {
		     $("td[name]", "table").hover(function ()
		         {
		             $(this).addClass("hover");
		         },
		         function ()
		         {
		             $("td[name]", "table").removeClass("hover");
		         });
		 }
	}
};

/**
 * 水平平铺tab
 */
CmpayUI.Tab =
{
	init: function (tabId,tabWidth,defaultNum,tabType)
	{
		tabWidth >= 1 ? tabWidth : 790;
		defaultNum >= 1 ? defaultNum : 1;
		var tabOptionNum = $("#"+tabId).find("li").length;//tab切换项目总数
		
		for(var i = 1;i <= tabOptionNum;i++ ){
			//tab选项设置属性tabItemIndex
			$("#"+tabId).find("li:nth-child("+i+")").find("a").attr("tabItemIndex",tabId+i);
			//tab选项绑定切换事件
			$("#"+tabId).find("li:nth-child("+i+")").find("a").click(function(){
				var objNm = $(this).attr("tabItemIndex");//点击对象tabItemIndex
				var tabUl = $("a[tabItemIndex="+objNm+"]").parent().parent();
				var optionNum =tabUl.find("li").length;//tab项总数
				var tabId = tabUl.attr("id");
				var boxId = "con_"+tabId;
				for(i=1;i<=optionNum;i++){
					tabUl.find("li:nth-child("+i+")").find("a").removeClass("on");
					$("#"+boxId).find("div:nth-child("+i+")").removeClass("u-displayBlock");
					$("#"+boxId).find("div:nth-child("+i+")").addClass("u-displayNone");
				}
				$("a[tabItemIndex="+objNm+"]").addClass("on");
				$("div[itemContent=con_"+objNm+"]").removeClass("u-displayNone");
				$("div[itemContent=con_"+objNm+"]").addClass("u-displayBlock");
			});
			//对应盒子的属性初始化
			$("#con_"+tabId).find("div:nth-child("+i+")").attr("itemContent","con_"+tabId+i);
			$("#con_"+tabId).find("div:nth-child("+i+")").addClass("u-displayNone");
			//设置选中效果
			if(defaultNum == i){
				$("#"+tabId).find("li:nth-child("+i+")").find("a").addClass("on");
				$("#con_"+tabId).find("div:nth-child("+i+")").removeClass("u-displayNone");
				$("#con_"+tabId).find("div:nth-child("+i+")").addClass("u-displayBlock");
			}
		}
		//设置tab宽度
		if(tabWidth != ""){
			$("#"+tabId).parent().css("width",tabWidth+"px");
		}
	}
};

/**
 * 初始化Tab
 */
function initUiTab()
{
    // 初始化上下折叠tab
    if ($(".u-letab .dd-on").length > 0)
    {
    	$(".u-letab .dd-on").prevAll("dt").removeClass("u-letab-menu-r-icon").addClass("u-letab-menu-d-icon dt-on").nextAll().show();
    }
    
    $(".u-letab-menu").click(function ()
    {
    	if ($(this).attr("class").indexOf("u-letab-menu-d-icon") >= 0)
    	{
    		$(this).removeClass("u-letab-menu-d-icon").addClass("u-letab-menu-r-icon").nextAll().hide();
    	}
    	else
    	{
    		if ($(this).attr("class").indexOf("no-extra") < 0)  // 无扩展菜单不需要更改背景和显示扩展菜单
    		{
    			$(this).removeClass("u-letab-menu-r-icon").addClass("u-letab-menu-d-icon").nextAll().show();
    		}
    		else
    		{
    		    $(".u-letab .dt-on").removeClass("dt-on");
    		    $(".u-letab .dd-on").removeClass("dd-on");
    		    $(this).addClass("dt-on");
    		}
    	}
    });
    
    $(".u-letab-menu-item").click(function ()
    {
        $(".u-letab .dt-on").removeClass("dt-on");
    	$(".u-letab .dd-on").removeClass("dd-on");
    	$(this).addClass("dd-on").prevAll("dt").removeClass("u-letab-menu-r-icon").addClass("u-letab-menu-d-icon dt-on").nextAll().show();
    });
    
    /**
     * 初始化上下无折叠tab
     */
    $(".u-ltab-menu a").click(function ()
    {
        $(".u-ltab-menu a").removeClass("on");
    	$(this).addClass("on");
    });
}

$(document).ready( function () {
	// 初始化单选复选框
     $("div").setRadio(); 
     $("div").setCheckBox();
     
     // 初始化tab
     initUiTab();
}); 
