/**
 * 获取url参数列表
 * @return {}
 */
function GetRequest()
{
   var url = location.search; //获取url中"?"符后的字串
   var theRequest = new Object();
   if(url.indexOf("?") != -1)
   {
        var str = url.substr(1);
       var strs = str.split("&");
        for(var i = 0; i < strs.length; i ++)
       {
            theRequest[strs[i].split("=")[0]]=unescape(strs[i].split("=")[1]);
       }
   }
   return theRequest;
}

/**
 * 页面导航初始化
 * @type 
 */
var uiType;
function init()
{
    var Request=new Object();
	Request=GetRequest();
	uiType = Request['type'];
			
	$(".nav-pills li").click(function ()
	{
	    if ($(this).attr("class") != "active")
	    {
	        $(".nav-pills li").removeClass("active");
	        $(this).addClass("active");
			        
	        $(".navDiv").hide();
	        $("#" + $(this).attr("id") + "Div").show();
	    }
	});
}

/**
 * 后处理
 */
function afterDo()
{
	// 代码着色
    dp.SyntaxHighlighter.HighlightAll("code", true, true);
}

/**
 * 运行脚本
 */
function runScrpit(id)
{
	var scriptText = $("#scriptText").val();
	
	try
	{
	    eval(scriptText);
	}
	catch (e)
	{
		alert("脚本错误");
	}
}

/**
 * 代码着色使用
 * @return {}
 */
String.prototype.capitalize = function()
{
    return this.charAt(0).toUpperCase() + this.substring(1).toLowerCase();
}
