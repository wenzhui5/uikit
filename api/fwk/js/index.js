$(document).ready(function ()
{
    $("#demoUI").click(function ()
    {
        CmpayUI.disable($("#demoUI"));
    });
    
	CmpayUI.ButtonGroup.init("gp1", [1, 2, 3, 4], 2); // 初始化选择按钮组
	
    CmpayUI.SelectDate.init("calendar");  // 初始化日历控件
    CmpayUI.Tab.init("tab1", "", 2,"");  // 初始化水平tab
	
	$('#err_input').error('错误!'); 
    var right = $('#right_input').right();
    $('#ul_s_control').selectControl();
    $('#table_s_control').tableControl();
    //$('#person_input_control').personbox();
    $('#record_table').pagenation({data:'load_data',form:'pageForm',callback:function(){alert('dddd');}});
	
	// 初始化日历控件
    $('#calendar1').calendar(
    {
        format: "yyyy-MM-dd"
    });
    
    // 初始化上下折叠tab
    if ($(".ui-letab .dd-on").length > 0)
    {
    	$(".ui-letab .dd-on").prevAll("dt").removeClass("ui-letab-menu-r-icon").addClass("ui-letab-menu-d-icon dt-on").nextAll().show();
    }
    
    $(".ui-letab-menu").click(function ()
    {
    	if ($(this).attr("class").indexOf("ui-letab-menu-d-icon") >= 0)
    	{
    		$(this).removeClass("ui-letab-menu-d-icon").addClass("ui-letab-menu-r-icon").nextAll().hide();
    	}
    	else
    	{
    		if ($(this).attr("class").indexOf("no-extra") < 0)  // 无扩展菜单不需要更改背景和显示扩展菜单
    		{
    			$(this).removeClass("ui-letab-menu-r-icon").addClass("ui-letab-menu-d-icon").nextAll().show();
    		}
    		else
    		{
    		    $(".ui-letab .dt-on").removeClass("dt-on");
    		    $(".ui-letab .dd-on").removeClass("dd-on");
    		    $(this).addClass("dt-on");
    		}
    	}
    });
    
    $(".ui-letab-menu-item").click(function ()
    {
        $(".ui-letab .dt-on").removeClass("dt-on");
    	$(".ui-letab .dd-on").removeClass("dd-on");
    	$(this).addClass("dd-on").prevAll("dt").removeClass("ui-letab-menu-r-icon").addClass("ui-letab-menu-d-icon dt-on").nextAll().show();
    });
});

/**
 * 打开弹出框
 */
function openWindow()
{
	$.dialog(
	{
	    id: "dialog",
	    content: "Hello World!",
	    title: "提示",
	    width: "500px",
	    height: "200px",
	    lock: true
	});
}

function initFloatLayer()
{
	setTimeout(function ()
	{
	    CmpayUI.FloatLayer.init(    // 初始化付层框
		{
		    id: "layerWindow",
		    target: "layerWindowTarget",
		    content: "内容",
		    width: 200,
		    height: 50,
		    top: 38,
		    left: -25
		});
	}, 100
	);
}