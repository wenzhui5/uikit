$(function(){
    $.fn.extend({
        bank:function(){
           return this.each(function(){
                var $this = $(this);
                var childs = $this.children();
                var $icon = $(childs[0]);
                var $single = $(childs[1]);
                var $link = $('.u-bank-link');
                initBankList($this);
                initBankBox();
                bindOtherBankList();
                bindBankLinkEvent($link);
                $icon.on('click',clickHandler);
                $icon.on('blur',blurHandler);
                $single.on('click',clickHandler);
                $single.on('blur',clickHandler);
           });
           /*更多银行连接事件绑定*/
           function bindBankLinkEvent($link){
                $link.children().each(function(){
                    $(this).on('click',function(){
                        $.dialog({id:'bank_dialog',title:'选择银行',width:'760px',height:'300px',content:$('#bank_box')[0]});
                    });    
                });
                
           }
           function bindOtherBankList(){
                var $listc = $('.u-ver-bank');
                var $banks = $listc.children('.ver-bank-list');
                bindBankListItemsEvent($banks);
           }
           /*初始化银行弹窗盒子*/
           function initBankBox(){
                var $bankBox = $('#bank_box');
                var $bankList = $($bankBox.children()[0]);
                $bankList.show();
                var $page = $('#bank_box').children().last();
                $bankBox.attr('data-page','1');
                $bankBox.attr('data-totalPage',$('#bank_box').children('.bank-box-list').length);
                bindBankListItemsEvent($bankList);
                bindBankPageEvent($page);
           }
           /*初始化银行下拉列表*/
           function initBankList($this){
               var $bankList = $($this.children()[3]);
               //var offset = $this.offset();
               //var w = $this[0].offsetWidth;
               //var height = $this[0].offsetHeight;
               $('body').append($bankList);
               //$bankList.offset({left:offset.left,top:offset.top+height-1});
               $bankList.attr('id',$this.attr('id')+'_bankList');
               bindBankListItemsEvent($bankList);
           }
           /*下拉银行列表选项事件绑定*/
           function bindBankListItemsEvent($list){
                var items = $list.children('a');
                items.each(function(){
                    $(this).on('click',itemClickHandler);        
                    $(this).hover(itemMouseoverHandler,itemMouseoutHandler);        
                });
           }
           /*下拉银行列表选项事件响应处理函数*/
           function bindBankPageEvent($page){
                var btns = $page.children();
                btns.each(function(i){
                    if( i == 0 ){
                        bindPrevPageEvent($(this));
                    }else if( i == btns.length - 1 ){
                        bindNextPageEvent($(this));
                    }else{
                        bindPageEvent($(this));
                    }        
                });
           }
           /*银行弹窗分页总的上一页事件绑定*/
           function bindPrevPageEvent($this){
                $this.on('click',clickHandler);
                $this.bind('mouseover',btnMouseoveHandler);
                $this.bind('mouseout',btnMouseoutHandler);
                function clickHandler(){
                    var $bankBox = $('#bank_box');
                    var curPage = $bankBox.attr('data-page');
                    curPage = parseInt(curPage);
                    if( curPage == 1 ) return;
                    curPage = curPage - 1;
                    loadPage(curPage);
                }
           }
           /*银行弹窗分页总的下一页事件绑定*/
           function bindNextPageEvent($this){
                $this.on('click',clickHandler);
                $this.bind('mouseover',btnMouseoveHandler);
                $this.bind('mouseout',btnMouseoutHandler);
                function clickHandler(){
                    var $bankBox = $('#bank_box');
                    var curPage = $bankBox.attr('data-page');
                    var totalPage = $bankBox.attr('data-totalPage');
                    totalPage = parseInt(totalPage);
                    curPage = parseInt(curPage);
                    if( curPage == totalPage ) return;
                    curPage = curPage + 1;
                    loadPage(curPage);
                }
           }
           /*银行弹窗分页总的下一页事件绑定*/
           function bindPageEvent($this){
                $this.on('mouseover',mouseoverHandler);
                $this.on('mouseout',mouseoutHandler);
                $this.on('click',clickHandler);
                function mouseoverHandler(e){
                    $(this).addClass('hover');
                }
                function mouseoutHandler(){
                    $(this).removeClass('hover');
                }
                function clickHandler(){
                    var curPage = $(this).text();
                    curPage = parseInt(curPage);
                    loadPage(curPage);

                }
           }
           /*分页中上一页，下一页鼠标滑过效果*/
           function btnMouseoveHandler(){
               $(this).addClass('btn-hover');
           }
           function btnMouseoutHandler(){
               $(this).removeClass('btn-hover');
           }
           /*加载分页的数据*/
           function loadPage(curPage){
               var $bankBox = $('#bank_box');
               var page = $bankBox.attr('data-page');
               page = parseInt(page);
               var lists = $bankBox.children('.bank-box-list');
               var $page = $bankBox.children('.page');
               var $pages = $page.children();
               var totalPage = $bankBox.attr('data-totalPage');
               totalPage = parseInt(totalPage);
               $($pages[page]).removeClass('active');
               $(lists[parseInt(page)-1]).hide();
               $(lists[parseInt(curPage)-1]).show();
               $bankBox.attr('data-page',curPage);
               $($pages[curPage]).addClass('active');
               bindBankListItemsEvent($(lists[parseInt(curPage)]));
               if(totalPage == curPage){
                   $($pages[parseInt(totalPage+1)]).addClass('disabled');
               }else if( $($pages[parseInt(totalPage+1)]).hasClass('disabled') ){
                   $($pages[parseInt(totalPage+1)]).removeClass('disabled');
               }
               if( curPage == 1 ){
                    $($pages[0]).addClass('disabled');
               }else if( $($pages[0]).hasClass('disabled') ){
                    $($pages[0]).removeClass('disabled');
               }
           }
           /*下拉银行选项点击事件*/
           function itemClickHandler(e){
                $this = $(this);
                var $bankList = $this.parent();
                var $bankInput = $('#bank_input');
                var $icon = $($bankInput.children()[0]);
                var $single = $($bankInput.children()[1]);
                var $input = $($bankInput.children()[2]);
                $icon.removeClass($input.val());
                $icon.addClass($this.val());
                $icon.addClass($this.attr('data-value'));
                $icon.attr('data-value',$this.attr('data-value'));
                $input.val($this.attr('data-value'));
                if( $bankList.hasClass('bank-list') ){
                    $bankList.hide();
                    $single.removeClass('open');
                }else if( $bankList.hasClass('bank-box-list') ){
                    $.dialog({id:'bank_dialog'}).close();
                }
           }
           /*银行选项高亮效果*/
           function itemMouseoverHandler(e){
                var $this = $(this);
                var $mask = $('<div></div>');
                $this.append($mask);
                $mask.addClass('orange-border-mask');
                $mask.attr('id','border_mask_'+$this.attr('data-value'));
           }
           function itemMouseoutHandler(e){
                var $this = $(this);
                var $mask = $($this.children()[1]);
                if( $mask.length>0 ){
                    $mask.remove();
                }
           }
           /*银行输入项点击事件*/
           function clickHandler(e){
                e.stopPropagation();
                var $inputC = $(this).parent();
                var $single = $($inputC.children()[1]);
                var $bankList = $('#'+$inputC.attr('id')+'_bankList');
                var offset = $inputC.offset();
               var w = $inputC[0].offsetWidth;
               var height = $inputC[0].offsetHeight;
               //这里用$bankList.offset();会出现为止不准确的情况
               $bankList.css('left',offset.left + 'px');
               $bankList.css('top',offset.top+height-1+ 'px');
                if( !$single.hasClass('open') ){
                    $bankList.show();
                    $single.addClass('open');
                    $(this).parent().addClass('focus');
                }else{
                    $single.removeClass('open');
                    $(this).parent().removeClass('focus');
                    $bankList.hide();
                }
                $bankList[0].onclick = function(e){
                    var e = window.event || e;
                    if( e.stopPropagation ){
                        e.stopPropagation();
                    }else{
                        e.cancelBubble = true;
                    }
                }
                document.onclick = function(){
                    $bankList.hide();
                    $($inputC.children()[1]).removeClass('open');
                    $inputC.removeClass('focus');
                }
           }
           function blurHandler(e){
                $(this).parent().removeClass('focus');
                var $inputC = $(this).parent();
                $($inputC.children()[1]).removeClass('open');
                var $bankList = $('#'+$(this).parent().attr('id')+'_bankList');
                $bankList.hide();
                e.stopPropagation();
           }
        }
    });        
})
