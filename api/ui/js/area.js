(function(){
var areastr = '010|北京|bei,jing;022|天津|tian,jin;021|上海|shang,hai;023|重庆|chong,qing;0311|石家庄|shi,jia,zhuang;0310|邯郸|han,dan;0319|邢台|xing,tai;0312|保定|bao,ding;0313|张家口|zhang,jia,kou;0314|承德|cheng,de;0315|唐山|tang,shan;0335|秦皇岛|qin,huang,dao;0317|沧州|cang,zhou;0316| 廊坊|lang,fang;0318|衡水|heng,shui;0791|南昌|nan,chang;0798|景德镇|jing,de,zhen;0790|新余|xin,yu;0792|九江|jiu,jiang;0701|鹰潭|ying,tan;0793|上饶|shang,rao;0795|宜春|yi,chun;0794|临川|lin,chuan;0796|吉安|ji,an;0797|赣州|gan,zhou;0531|济南|ji,nan;0532|青岛|qing,dao;0533|淄博|zi,bo;0536|潍坊|wei,fang;0535|烟台|yan,tai;0631|威海|wei,hai;0537|兖州|yan,zhou;0633|日照|ri,zhao;0534|德州|de,zhou;0530|郓城县|yun,cheng,xian;0538|泰安|tai,an;0351|太原|tai,yuan;0352|大同|da,tong;0353|阳泉|yang,quan;0355|长治|chang,zhi;0349|朔州|shuo,zhou;0354|榆次|yu,ci;0358|孝义|xiao,yi;0357|临汾|lin,fen;0359|运城|yun,cheng;0471|呼和浩特|hu,he,hao,te;0472|包头|bao,tou;0476|巴林左旗|ba,lin,zuo,qi;0479|二连浩特|er,lian,hao,te;0470|满洲里|man,zhou,li;0475|通辽|tong,liao;0477|准格尔旗|zhun,ge,er,qi;0482|乌兰浩特|wu,lan,hao,te;0371|郑州|zheng,zhou;0378|开封|kai,feng;0379|洛阳|luo,yang;0373|新乡|xin,xiang;0393|濮阳|pu,yang;0370|商丘|shang,qiu;0377|南阳|nan,yang;0394|周口|zhou,kou;0396|汝南县|ru,nan,xian;024|沈阳|shen,yang;0411|大连|da,lian;0412|鞍山|an,shan;0413|抚顺|fu,shun;0414|本溪|ben,xi;0415|丹东|dan,dong;0416|锦州|jin,zhou;0417|营口|ying,kou;0418|阜新|fu,xin;0419|辽阳|liao,yang;0410|铁岭|tie,ling;027|武汉|wu,han;0714|黄石|huang,shi;0710|襄樊|xiang,fan;0719|十堰|shi,yan;0717|宜昌|yi,chang;0714|荆门|jing,men;0712|孝感|xiao,gan;0713|黄冈|huang,gang;0718|恩施|en,shi;0716|荆沙|jing,sha;0431|长春|chang,chun;0432|吉林|ji,lin;0434|四平|si,ping;0437|辽源|liao,yuan;0435|通化|tong,hua;0439|临江|lin,jiang;0436|大安|da,an;0433|敦化|dun,hua;0440|珲春|hun,chun;0731|长沙|chang,sha;0733|株州|zhu,zhou;0732|湘潭|xiang,tan;0734|衡阳|heng,yang;0730|岳阳|yue,yang;0736|常德|chang,de;0735|郴州|chen,zhou;0737|益阳|yi,yang;0746|冷水滩|leng,shui,tan;0745|怀化|huai,hua;0744|张家界|zhang,jia,jie;0451|哈尔滨|ha,er,bin;0452|齐齐哈尔|qi,qi,ha,er;0459|大庆|da,qing;0458|伊春|yi,chun;0453|牡丹江|mu,dan,jiang;0454|佳木斯|jia,mu,si;0455|缓化|huan,hua;0457|漠河县|mo,he,xian;0456|黑河|hei,he;020|广州|guang,zhou;0755|深圳|shen,zhen;0756|珠海|zhu,hai;0754|汕头|shan,tou;0751|韶关|shao,guan;0752|惠州|hui,zhou;0769|东莞|dong,guan;0760|中山|zhong,shan;0757|佛山|fo,shan;0759|湛江|zhan,jiang;025|南京|nan,jing;0516|徐州|xu,zhou;0518|连云港|lian,yun,gang;0517|淮安|huai,an;0527|宿迁|su,qian;0515|盐城|yan,cheng;0514|扬州|yang,zhou;0513|南通|nan,tong;0511|镇江|zhen,jiang;0519|常州|chang,zhou;0510|无锡|wu,xi;0512|苏州|su,zhou;0520|常熟|chang,shu;0771|南宁|nan,ning;0772|柳州|liu,zhou;0773|桂林|gui,lin;0774|梧州|wu,zhou;0779|北海|bei,hai;0777|钦州|qin,zhou;0898|海口|hai,kou;0899|三亚|san,ya;0890|儋州|dan,zhou;028|成都|cheng,du;0812|攀枝花|pan,zhi,hua;0838|德阳|de,yang;0816|绵阳|mian,yang;0813|自贡|zi,gong;0832|内江|nei,jiang;0833|乐山|le,shan;0830|泸州|lu,zhou;0831|宜宾|yi,bin;0571|杭州|hang,zhou;0574|宁波|ning,bo;0573|嘉兴|jia,xing;0572|湖州|hu,zhou;0575|绍兴|shao,xing;0579|金华|jin,hua;0570|衢州|qu,zhou;0580|舟山|zhou,shan;0577|温州|wen,zhou;0576|台州|tai,zhou;0851|贵阳|gui,yang;0852|遵义|zun,yi;0853|安顺|an,shun;0858|六盘水|liu,pan,shui;0551|合肥|he,fei;0554|淮南|huai,nan;0552|蚌埠|bang,bu;0555|马鞍山|ma,an,shan;0556|安庆|an,qing;0559|黄山|huang,shan;0550|滁州|chu,zhou;0557|宿州|su,zhou;0565|巢湖|chao,hu;0563|宣州|xuan,zhou;0871|昆明|kun,ming;0870|昭通|zhao,tong;0874|曲靖|qu,jing;0877|江川|jiang,chuan;0879|思茅|si,mao;0888|丽江县|li,jiang,xian;0873|开远|kai,yuan;0878|楚雄|chu,xiong;0591|福州|fu,zhou;0592|厦门|sha,men;0598|三明|san,ming;0594|莆田|pu,tian;0595|泉州|quan,zhou;0596|漳州|zhang,zhou;0599|南平|nan,ping;0593|宁德|ning,de;0597|龙岩|long,yan;029|西安|xi,an;0919|铜川|tong,chuan;0917|宝鸡|bao,ji;0913|渭南|wei,nan;0914|商州|shang,zhou;0891|拉萨|la,sa;0892|日喀则|ri,ka,ze;08018|仁布县|ren,bu,xian;08059|丁青县|ding,qing,xian;0897|阿里地区|a,li,di,qu;0931|兰州|lan,zhou;0935|金昌|jin,chang;0938|天水|tian,shui;0933|平凉|ping,liang;0937|玉门|yu,men;0937|敦煌|dun,huang;0971|西宁|xi,ning;0972|平安县|ping,an,xian;0979|格尔木|ge,er,mu;0975|玛沁县|ma,qin,xian;0951|银川|yin,chuan;0952|石嘴山|shi,zui,shan;0953|青铜峡|qing,tong,xia;0954|海原县|hai,yuan,xian;0991|乌鲁木齐|wu,lu,mu,qi;0990|克拉玛依|ke,la,ma,yi;0995|吐鲁番|tu,lu,fan;0998|喀什|ka,shi;0908|阿图什|a,tu,shi;0996|库尔勒|ku,er,le;00852|香港|xiang,gang;00853|澳门|ao,men;00886|台湾|tai,wan';





function Area(area){
    this.id = area.id;
    this.name = area.name;
    this.pinyin = area.pinyin;
}

function pushArea(){
    var areas = [];
    var str = areastr;
    var a = str.split('\;');
    for( var i = 0 ; i < a.length ; i++ ){
        var city = a[i].split('\|');
        var area = new Area({id:city[0],name:city[1],pinyin:city[2]});
        areas.push(area);
    }
    return areas;
}
var citys = pushArea();
window.CmpayUI = window.CmpayUI || {};
window.CmpayUI.citys = citys;
})();
